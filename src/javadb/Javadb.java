/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package javadb;

/**
 *
 * @author Idris Dzulfikar
 */
public class Javadb {

static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:127.0.0.1/tugas"; 
    static final String USER = "root"; 
    static final String PASS = ""; 

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Class.forName(JDBC_DRIVER);

            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);

            int choice;
            do {
                showMenu();
                choice = scanner.nextInt();
                scanner.nextLine(); 
                switch (choice) {
                    case 1:
                        insertData(conn);
                        break;
                    case 2:
                        updateData(conn);
                        break;
                    case 3:
                        deleteData(conn);
                        break;
                    case 4:
                        showData(conn);
                        break;
                    case 5:
                        System.out.println("Exiting...");
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.");
                }
            } while (choice != 5);

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showMenu() {
        System.out.println("\n--- Menu ---");
        System.out.println("1. Insert Data");
        System.out.println("2. Update Data");
        System.out.println("3. Delete Data");
        System.out.println("4. Show Data");
        System.out.println("5. Exit");
        System.out.print("Enter your choice: ");
    }

    public static void insertData(Connection conn) {
        try {
            System.out.print("Enter peminjam: ");
            String peminjam = scanner.nextLine();
            System.out.print("Enter tanggal (YYYY-MM-DD): ");
            String tanggal = scanner.nextLine();
            System.out.print("Enter namabuku: ");
            String namabuku = scanner.nextLine();

            String query = "INSERT INTO borrow_records (peminjam, tanggal, namabuku) VALUES (?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, peminjam);
            pstmt.setDate(2, Date.valueOf(tanggal));
            pstmt.setString(3, namabuku);
            pstmt.executeUpdate();
            System.out.println("Data inserted successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateData(Connection conn) {
        try {
            showData(conn);
            System.out.print("Enter the id of data to update: ");
            int id = scanner.nextInt();
            scanner.nextLine(); 
            System.out.print("Enter new peminjam: ");
            String peminjam = scanner.nextLine();
            System.out.print("Enter new tanggal (YYYY-MM-DD): ");
            String tanggal = scanner.nextLine();
            System.out.print("Enter new namabuku: ");
            String namabuku = scanner.nextLine();

            String query = "UPDATE borrow_records SET peminjam = ?, tanggal = ?, namabuku = ? WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, peminjam);
            pstmt.setDate(2, Date.valueOf(tanggal));
            pstmt.setString(3, namabuku);
            pstmt.setInt(4, id);
            pstmt.executeUpdate();
            System.out.println("Data updated successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteData(Connection conn) {
        try {
            showData(conn);
            System.out.print("Enter the id of data to delete: ");
            int id = scanner.nextInt();
            scanner.nextLine(); 

            String query = "DELETE FROM borrow_records WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            System.out.println("Data deleted successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void showData(Connection conn) {
        try {
            String query = "SELECT * FROM borrow_records";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            System.out.println("\n--- Data List ---");
            while (rs.next()) {
                int id = rs.getInt("id");
                String peminjam = rs.getString("peminjam");
                Date tanggal = rs.getDate("tanggal");
                String namabuku = rs.getString("namabuku");
                System.out.println(id + ": " + peminjam + " - " + tanggal + " - " + namabuku);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
